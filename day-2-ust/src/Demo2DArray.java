
public class Demo2DArray {

	public static void main(String[] args) {
		
		int a[]={3,5,7,8};
		//print it

//		for(int i=0;i<a.length; i++){
//		 System.out.println(a[i]);
//		}
		//enhace for loop
//		for(int temp: a) {
//			System.out.println(temp);
//		}
//		
		
		int x[][]={{1,0,0},{0,1,0},{0,0,1}};
		
		for(int temp[]:x){

			for(int temp2: temp) {
				System.out.print(temp2);
			}
			System.out.println();
		}
	}
}
