import java.util.Scanner;

public class DemoPrimeNo {

	public static void main(String[] args) {
		//check for prime no
		Scanner scanner=new Scanner(System.in);
		System.out.println("PE an no to check for prime no");
		int no=scanner.nextInt();
		boolean isPrime=isPrimeNumber(no);//let i assure no is a prime no
		
		
		if(isPrime) {
			System.out.println("it is a prime no");
		}
		else
			System.out.println("it is not a prime no");
	}

	public static boolean isPrimeNumber(int no) {
		boolean isPrime=true;
		
		for(int i=2; i<=(no-1); i++) {
			if(no%i==0) {
				isPrime=false;
				break;
			}
		}
		return isPrime;
	}
}
