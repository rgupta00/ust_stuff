
public class LinearSearch {
	
	public static void main(String[] args) {
		int arr[]= {3,5,6,-3,78,22};
		int val=-3;
		int pos=-1;//invalid pos
		
		pos = findElementByLinearSearch(arr, val);
		
		if(pos!=-1) {
			System.out.println("elment is found and index is : "+pos);
		}else
			System.out.println("element is not found");
	}

	private static int findElementByLinearSearch(int[] arr, int val) {
		int pos=-1;
		for(int i=0;i<arr.length; i++) {
			if(arr[i]==val) {
				pos=i;
				break;
			}
		}
		return pos;
	}

}
