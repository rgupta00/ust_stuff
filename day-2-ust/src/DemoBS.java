
public class DemoBS {

	public static void main(String[] args) {
		int arr[]= {3,5,6,83,98,202};
		int first,last, mid;
		first=0;
		last=arr.length-1;
		 mid = (first + last) / 2;
		 
		 int key=83;
		while (first <= last) {
			if (arr[mid] < key) {
				first = mid + 1;
			} else if (arr[mid] == key) {
				System.out.println("Element is found at index: " + mid);
				break;
			} else {
				last = mid - 1;
			}
			mid = (first + last) / 2;
		}
		if (first > last) {
			System.out.println("Element is not found!");
		}
	}
}
