
class A {
	private int id;

	// how to remove duplicate code from the ctr call?
	// "init block"

	//static init block
	static {
		System.out.println("static init block: at the time of class loading by JVM");
	}
	// init block
	{
		System.out.println("common code 2");
	}
	
	{
		System.out.println("common code");
	}

	
	
	A() {
		System.out.println("default ctr");
	}

	public A(int id) {
		System.out.println("parameterized ctr");
		this.id = id;
	}

}

public class InitBlockDemo {

	public static void main(String[] args) {
		A a=new A(55);
		A a2=new A();
		
	
	}

}
