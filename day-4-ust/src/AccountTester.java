
public class AccountTester {

	public static void main(String[] args) {
		Account account=new Account(11, "amit", 5000);
		account.accountDetail();
			
		account.withdraw(700);
		account.accountDetail();
		
		account.deposit(7000);
		account.accountDetail();
	}
}
