
public class DemoFabSer {

	public static void main(String[] args) {
		int sum =getSumOfSeries(6);
		System.out.println("sum : " + sum);
	}
	
	public static int getSumOfSeries(int n) {
		int n1, n2, n3, sum;
		n1 = 0;
		n2 = 1;

		sum = 1;
		// 0 1 1 2 3 5
		for (int i = 2; i <= 5; i++) {
			n3 = n1 + n2;
			sum = sum + n3;
			n1 = n2;
			n2 = n3;

		}
		return sum;
	}
}
