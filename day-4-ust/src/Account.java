//ctr + shift+ f
public class Account {
	private int id;
	private String name;
	private double balance;

	public Account() {

	}

	// constructor: used to inilized the instance variable
	public Account(int id, String name, double balance) {
		this.id = id;
		this.name = name;
		this.balance = balance;
	}

	// final bal can not be -ve
	public void withdraw(int amount) {
		double tempBalance = balance - amount;
		if (tempBalance < 0) {
			System.out.println("you dont have sufficient amount");
		} else {
			balance = tempBalance;
		}

	}

	// 5L
	public void deposit(int amount) {
		double tempBalance = balance + amount;
		if (tempBalance > 500000) {
			System.out.println("you need to open current account");
		} else {
			balance = tempBalance;
		}

	}

	public double getBalance() {
		return balance;
	}

	public void accountDetail() {
		System.out.println("id : " + id);
		System.out.println("name : " + name);
		System.out.println("balance : " + balance);
	}
}
