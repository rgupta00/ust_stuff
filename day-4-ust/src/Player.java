
public class Player {
	//instance varaiable: different for each object
	private int id;
	private String name;
	private int rank;
	
	
	//static vs final are not the same thing
	
	//static data is common to all object
	private static int playerCount=0;
	
	//static method ?
	//normal method : instance method
	
	//static method : they can be called with the name of the class
	public static void printPlayerCounter() {
		//static method can access static data but can not access instance data
		//static method dont have accessablity of "this" ref
		//System.out.println(id);
		
		System.out.println(playerCount);
	}
	
	//init block?
	
	public Player() {
		//some common code
	}
	
	public Player( String name, int rank) {
		//some common code
		this.id = ++playerCount;
		this.name = name;
		this.rank = rank;
	}
	
	public void print() {
		System.out.println("------------------");
		System.out.println("id: "+ id);
		System.out.println("name: "+ name);
		System.out.println("rank: "+ rank);
		System.out.println("------------------");
	}
	
}
