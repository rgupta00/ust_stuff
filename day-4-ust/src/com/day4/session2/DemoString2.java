package com.day4.session2;

public class DemoString2 {

	public static void main(String[] args) {
		
		//String vs StringBuilder vs StringBuffer
		//what is the diff?
		//String are immutble and thread safe and stored in String pool and go control of GC
		
		
		//mutable, they are saved in the thread pool
		//StringBuilder: 1.5 , not thread safe but good in perforamce 
		//StringBuffer: older and thead safe but good in performance , not that used now a day
		
//		StringBuilder sb=new StringBuilder("hello");
//		System.out.println(sb.toString());
		
//		//Strings are immutable?
//		String a="hello";
//		a=a.toUpperCase();
//		System.out.println(a);
	}
}
