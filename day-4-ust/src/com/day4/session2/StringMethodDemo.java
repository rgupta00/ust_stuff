package com.day4.session2;

public class StringMethodDemo {

	public static void main(String[] args) {

		String data = "Muhammad Ali was an American professional boxer, activist, entertainer and philanthropist";
		
		String tokens[]=data.split(" ");
		
		for(String token: tokens) {
			if(!token.equals("American"))
			System.out.println(token.toUpperCase());
		}
		
//		String s1="raj";
//		String s2="RAJ";
//		s1=s1.toUpperCase();
//		if(s1.equals(s2)) {
//			System.out.println("string have equal contents");
//		}else {
//			System.out.println("string not hv equal contents");
//		}

		// 0123456789
//		String data="i love java and java love me";
//		
//		if(data.contains("lve")) {
//			System.out.println("data contain love word");
//		}else
//			System.out.println("not contains");

//		System.out.println(data.charAt(5));//index=5 then pos=6
//		System.out.println(data.length());
//		
//		System.out.println(data.substring(2));
//		System.out.println(data.subSequence(2, 10));//love
	}

}
