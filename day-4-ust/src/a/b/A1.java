package a.b;

//default visibilty of a class, package private
//public 
 public class A1 {
	private int i=7;
	
	int j=77;
	protected int k=4;
	public int m=111;
	

	protected void fooA1Protected() {
		System.out.println("fooA1Protected");
	}

	 void fooA1Default() {
		System.out.println("fooA1Default");
	}
	
	
	public void fooA1() {
		System.out.println("A1 of package a.b"+i);
	}
}
