package a.c;

import a.b.*;

public class A2 extends A1{
	public static void main(String[] args) {
		
		
		A2 a1=new A2();
		a1.fooA1Protected();
		a1.fooA1Default();
	
//		//diff bw protected and default method?
//		
//		//"the diff is that protected method /data can be access iff the class
//		//in other package subclass it"
//		//but default data/method can not be access in any way in other package
//		A2 a=new A2();
//		a.fooA1Protected();
//		a.fooA1Default();
	}
}
