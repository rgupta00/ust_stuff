
public class PlayerTester {

	public static void main(String[] args) {
		Player player=new Player("sachine", 1);
		player.print();
		
		Player player2=new Player("foo", 19);
		player2.print();
		
		Player player3=new Player("bar", 900);
		player3.print();
		
		Player.printPlayerCounter();
	}
}
