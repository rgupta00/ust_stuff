
class Employee{
	private int id;
	private String name;
	private double salary;
	
	//getter and seters
	//getter are used to get the value: immutator
	
	public String getName() {
		return name;
	}
	
	public int getId() {
		return id;
	}
	public double getSalary() {
		return salary;
	}
	
	//setter is used to set the value : mutator
	
	public void setName(String name) {
		this.name=name;
	}
	
	public void setId(int id) {
		this.id=id;
	}
	public void setSalary(double salary) {
		this.salary=salary;
	}
	
	public Employee() {	
	}
	
	public Employee(int id, String name, double salary) {
		this.id=id;
		this.name=name;
		this.salary=salary;
	}
	
	public void printEmployeeDetails() {
		this.id=id;
		this.name=name;
		this.salary=salary;
		System.out.println("id : "+ id);
		System.out.println("name : "+ name);
		System.out.println("salary : "+ salary);
	}
}

public class EmployeeDemo {
	
	public static void main(String[] args) {
		Employee employee=new Employee(4, "rajat", 70);
	
		employee.printEmployeeDetails();
		
		employee.setSalary(80);
		employee.printEmployeeDetails();
		
		System.out.println(employee.getName());
		
	}

}
