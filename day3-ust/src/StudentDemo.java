
public class StudentDemo {

	public static void main(String[] args) {
		Student student=new Student(45, "krishna", "Java Training ust", 1);
		student.printStudentDetails();
		student.setYear(3);
		
		student.printStudentDetails();
		System.out.println("------------------");
		Student student2=new Student(student);
		student2.printStudentDetails();
		
		System.out.println("------------------");
		Student student3=new Student();
		student3.printStudentDetails();
	}
}
