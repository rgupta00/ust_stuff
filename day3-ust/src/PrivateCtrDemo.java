//Singleton design pattern: i want 1 object per application*
class Foo{
	private static Foo foo=new Foo();
	
	private Foo() {
		System.out.println("dare to call me!");
	}
	
	public static Foo getFoo() {
		return foo;
	}
	//static method : u dont need object of that class to call this method!
	
}
public class PrivateCtrDemo {
	
	public static void main(String[] args) {
		
		Foo foo=Foo.getFoo();
		
		
	}

}
