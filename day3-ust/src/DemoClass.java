
class A{
	private int i;//instance varaible, each object would hv separate copy of it
	private int j;//instance varaible, each object would hv separate copy of it
	
	public A() {
		System.out.println("i am ctr of class A");
	}
	
	public A(int i, int j) {
		int p=0;//local variable
		System.out.println("parameterized ctr");
		
		//THIS REFER TO INSTANCE VARAIBEL
		this.i=i;//we are assigning the value of local variable to instance variable
		this.j=i;//local variable is assigned to local varaible , ie assignment have no effect
	}
	
	public void doWork() {
		System.out.println("i am doing work: "+ i+ " : "+ j);
	}
}
public class DemoClass {
	public static void main(String[] args) {
		A a=new A(44,3);
		a.doWork();
		
		A a2=new A(404,2);
		a2.doWork();
		
	

	}
}
