
public class Student {
	private int id;
	private String name;
	private String course;
	private int year;
	
	//default
	public Student() {
		this(34,"foo","BTech",4);
	}
	
	//para
	 public Student(int id, String name, String course, int year) {
		this.id = id;
		this.name = name;
		this.course = course;
		this.year = year;
	}
	//copy
	public Student(Student student) {
		this.id = student.id;
		this.name = student.name;
		this.course = student.course;
		this.year = student.year;
	}

	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
	
	public void printStudentDetails() {
		System.out.println("id: "+ id);
		System.out.println("name: "+ name);
		System.out.println("course: "+ course);
		System.out.println("year: "+ year);
	}
	
	
}
